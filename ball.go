package main

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

type Ball struct {
	xPos, yPos int
	xMov, yMov float64
	speed      float64
}

func (b *Ball) reflectX() {
	b.xMov *= -1.
}

func (b *Ball) reflectY() {
	b.yMov *= -1.
}

func (b *Ball) Update() {
	xInc := b.speed * b.xMov
	yInc := b.speed * b.yMov

	b.xPos += int(xInc)

	if b.yPos > 0 && b.yPos <= scrHeight {
		b.yPos += int(yInc)
	} else {
		b.reflectY()
		b.yPos -= int(yInc)
	}
}

func (b *Ball) Draw(screen *ebiten.Image) {
	ebitenutil.DrawRect(screen, float64(b.xPos), float64(b.yPos), ballRadius, ballRadius, color.RGBA{0, 0, 0xff, 0xff})
}

func (b *Ball) OutOfBounds() int {
	if b.xPos >= scrWidth {
		return 2
	} else if b.xPos <= 0 {
		return 1
	}
	return 0
}
