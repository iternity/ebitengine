package main

import (
	"errors"
	"fmt"
	"image/color"
	_ "image/png"
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

var paddleLeft *Paddle
var paddleRight *Paddle
var ball *Ball
var scoreLeft, scoreRight int

const (
	ballSpeed    = 2.
	ballRadius   = 5.
	paddleSpeed  = 3.
	paddleWidth  = 15.
	paddleHeight = 20.
	paddingSide  = 20
	scrWidth     = 320
	scrHeight    = 240
)

func init() {
	paddleLeft = &Paddle{
		xPos:    paddingSide,
		yPos:    scrHeight/2 - paddleHeight/2,
		speed:   paddleSpeed,
		control: NewControl(ebiten.KeyW, ebiten.KeyS),
	}
	paddleRight = &Paddle{
		xPos:    scrWidth - 20 - paddleWidth,
		yPos:    scrHeight/2 - paddleHeight/2,
		speed:   paddleSpeed,
		control: NewControl(ebiten.KeyUp, ebiten.KeyDown),
	}
	ball = newBall()
	scoreLeft = 0
	scoreRight = 0
}

type Game struct{}

func newBall() *Ball {
	return &Ball{
		xPos:  scrWidth/2 - int(ballRadius)/2,
		yPos:  scrHeight/2 - int(ballRadius)/2,
		xMov:  0.5,
		yMov:  0.5,
		speed: ballSpeed,
	}
}

func (g *Game) Update() error {
	paddleLeft.Update()
	paddleRight.Update()
	ball.Update()
	paddleLeft.checkCollision(ball)
	paddleRight.checkCollision(ball)
	if ebiten.IsKeyPressed(ebiten.KeyEscape) {
		return errors.New("Quit game!")
	}
	switch ball.OutOfBounds() {
	case 2:
		scoreLeft += 1
		ball = newBall()
	case 1:
		scoreRight += 1
		ball = newBall()
	}
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	screen.Fill(color.RGBA{0, 0xff, 0, 0xff})
	paddleLeft.Draw(screen)
	paddleRight.Draw(screen)
	ball.Draw(screen)
	ebitenutil.DebugPrint(screen, fmt.Sprintf("Score: %d - %d", scoreLeft, scoreRight))
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return scrWidth, scrHeight
}

func main() {
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Hello, world!")
	if err := ebiten.RunGame(&Game{}); err != nil {
		log.Fatal(err)
	}
}
